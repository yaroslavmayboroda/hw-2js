"use script"

let name = prompt("Введите ваше имя:");
let age = parseInt(prompt("Введите ваш возраст:"));
while (!name || isNaN(age)) {
    name = prompt("Пожалуйста, введите ваше имя:");
    age = parseInt(prompt("Пожалуйста, введите ваш возраст:"));
}
if (age < 18) {
    alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
    const confirmation = confirm("Are you sure you want to continue?");
    if (confirmation) {
        alert("Welcome, " + name + "!");
    } else {
        alert("You are not allowed to visit this website?");
    }
} else {
    alert("Welcome, " + name + "!");
}
